﻿using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace BelarusBank
{
    class Connection
    {
        private string ToConnect(string url)
        {
            string response;

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                response = streamReader.ReadToEnd();
            }
            return response;
        }

        public List<ModelOfJsonObject> SetUrl(string city)
        {
            string url = "https://belarusbank.by/api/atm?city=" + city;
            string response = ToConnect(url);

            List<ModelOfJsonObject> content = JsonConvert.DeserializeObject<List<ModelOfJsonObject>>(response);

            return content;
        }
    }
}
