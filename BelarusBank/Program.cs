﻿using System;

namespace BelarusBank
{
    class Program
    {
        static void Main(string[] args)
        {
            string userCity = "Минск";
            string userStreet = "Независимости";
            DataProcessing get = new DataProcessing(userCity);

            get.GetATMInfo(userStreet);

            Console.ReadKey();
        }
    }
}
