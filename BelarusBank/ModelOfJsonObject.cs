﻿
namespace BelarusBank
{
    public class ModelOfJsonObject
    {
        public string id { get; set; }
        public string area { get; set; }
        public string city_type { get; set; }
        public string city { get; set; }
        public string address_type { get; set; }
        public string address { get; set; }
        public string house { get; set; }
        public string install_place { get; set; }
        public string work_time { get; set; }
        public string gps_x { get; set; }
        public string gps_y { get; set; }
        public string install_place_full { get; set; }
        public string work_time_full { get; set; }
        public string ATM_type { get; set; }
        public string ATM_error { get; set; }
        public string currency { get; set; }
        public string cash_in { get; set; }
        public string ATM_printer { get; set; }
    }
}
