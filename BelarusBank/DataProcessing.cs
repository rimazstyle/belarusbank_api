﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace BelarusBank
{
    class DataProcessing
    {
        ModelOfJsonObject model;
        Connection connection;
        string city; 

        public DataProcessing(string city)
        {
            model = new ModelOfJsonObject();
            connection = new Connection();
            this.city = city;
        }

        public void GetATMInfo(string street)
        {
            List<ModelOfJsonObject> data = connection.SetUrl(city);
            bool flag = true;

            foreach (var item in data)
            {
                if (item.address == street)
                {
                    flag = false;
                    Console.WriteLine($"Город {item.city}\nАдресс {item.address_type} {item.address} {item.house}\n" +
                    $"Место установки {item.install_place}\nВыдаваемая валюта {item.currency} Режим работы {item.work_time}\n\n");
                }
            }
            if (flag)
            {
                Console.WriteLine("На такой улице банкоматов не найдено");
            }
        }
    }
}
